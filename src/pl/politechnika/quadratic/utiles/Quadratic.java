package pl.politechnika.quadratic.utiles;

public class Quadratic {

    private double a;
    private double b;
    private double c;
    private double x;

    public Quadratic(double a, double b, double c, double x) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.x = x;
    }

    public Quadratic(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    private double Delta() {
        double result = (Math.pow(b, 2) - (4*a*c));
        return result;
    }

    public double QuadraticFirstZero() {
        double result = ((-b)-Math.sqrt(Delta()));
        result=(result/(2*a));
        return result;
    }
    public double QuadraticSecundZero() {
        double result = ((-b)+Math.sqrt(Delta()));
        result=(result/(2*a));
        return result;
    }
    public double getQuadraticFunction() {

        double result = (a*Math.pow(x, 2))+(b*x)+c;


        return result;
    }

    @Override
    public String toString() {
        return "Quadratic{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", x=" + x +
                '}';
    }
}
