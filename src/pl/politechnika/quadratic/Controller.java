package pl.politechnika.quadratic;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private TextField number_a;

    @FXML
    private TextField number_b;

    @FXML
    private TextField number_c;

    @FXML
    private Button drawButton;

    public Button getDrawButton() {
        return drawButton;
    }
}
